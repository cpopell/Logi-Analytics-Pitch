# Introduction

GitLab is a self-hosted GitHub alternative. It is a Git front-end that includes 
a number of features that allow for simpler collaboration, communication, and community interaction 
(either in a professional or social context).

https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/doc/gitlab-mattermost